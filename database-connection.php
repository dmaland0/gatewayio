<?php

	class DatabaseConnection {
		//Configuration
		//Edit this section in accordance with the setup of your MySQL database.
		private $servername = "localhost";
		private $username = "gateway";
		private $password = "P{}0h4wz9";
		private $dbname = "gateway";
		//End Configuration

		public $connection = null;
		public $connection_details = null;

		function __construct() {

			try {
				$this->connection = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
				$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->connection_details = "<h1 class=\"connection\">Connected successfully to the database \"$this->dbname.\"</h1>";
			}

			catch(PDOException $e) {
				$this->connection = false;
				$this->connection_details = "Connection failed. The database server may not be reachable, or there may be a problem with the configuration section of database-connection.php.\n\nMore information: " . $e->getMessage();
			}

		}

	}

?>