<?php

	$root_directory = $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/throttling-logic.php';
	include_once $root_directory . '/../logic/random-string-generator.php';
	include_once $root_directory . '/../logic/password-hasher.php';
	include_once $root_directory . '/../logic/email-sender.php';

	class PasswordReset {

		private $connection;
		private $result;

		public function reset($email) {
			$throttling_logic = new ThrottlingLogic();
			$throttling_result = $throttling_logic->throttle("password-reset");

			if ($throttling_result == "Proceed") {
				$query = $this->connection->prepare("SELECT * FROM users WHERE email = ?");
				$query->execute(array($email));
				$user = $query->fetch(PDO::FETCH_OBJ);

				if ($user) {
					$raw_new_password = RandomStringGenerator::generate(20);
					$hashed_password = PasswordHasher::hashPassword($raw_new_password,$user->salt);

					$action = $this->connection->prepare("UPDATE users SET password = ?, must_change_password = ? WHERE id = ?");
					$action->execute(array($hashed_password, 1, $user->id));

					$email_sent = EmailSender::send($user->email, "Password Reset", "Your password has been reset to this: $raw_new_password\r\nFor security, you will be required to change your password immediately upon your next login.");

					$this->result['success'] = true;
					$this->result['message'] = "You have been given a temporary password, and the system has attempted to send you an email with that password.";
					return $this->result;
				} else {
					$this->result['success'] = false;
					$this->result['message'] = "Sorry, that email doesn't appear to belong to a registered user on this system.";
					return $this->result;
				}

			} else {
				$this->result['success'] = false;
				$this->result['message'] = $throttling_result;
				return $this->result;
			}

		}

		function __construct() {
			$database_connection = new DatabaseConnection();
			$this->connection = $database_connection->connection;
		}

	}

?>
