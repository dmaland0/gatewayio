<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';

	class PasswordComplexityConfiguration {
	
		public $minimum_characters;
		public $minimum_capital_letters;
		public $minimum_numbers;
		public $minimum_special_characters;
		
		function __construct() {
			//These are defaults, in case the configuration in the database is not set.
			$this->minimum_characters = 1;
			$this->minimum_capital_letters = 0;
			$this->minimum_numbers = 0;
			$this->minimum_special_characters = 0;

			$database_connection = new DatabaseConnection();
			$connection = $database_connection->connection;

			$configuration_query = $connection->query("SELECT * FROM password_complexity_configuration");
			$configuration_data = $configuration_query->fetch(PDO::FETCH_OBJ);

			if (!empty($configuration_data)) {
				
				$this->minimum_characters = $configuration_data->minimum_characters;
				$this->minimum_capital_letters = $configuration_data->minimum_capital_letters;
				$this->minimum_numbers = $configuration_data->minimum_numbers;
				$this->minimum_special_characters = $configuration_data->minimum_special_characters;

			}

		}
	}

?>