<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/password-hasher.php';
	include_once $root_directory . '/../logic/password-validation-logic.php';

	class PasswordChangeLogic {
		private $connection;
		public $problems;

		public function changePassword($user_id, $password, $repeat_password) {
			$password_validation = new PasswordValidationLogic();
			$this->problems = $password_validation->validate($password);

			if ($password != $repeat_password) {
				array_push($this->problems, "The \"New Password\" and \"Repeat New Password\" fields don't match.");
			}

			if (count($this->problems) > 0) {
				return false;
			}

			$action = $this->connection->prepare("SELECT * FROM users WHERE id=?");
			$action->execute(array($user_id));
			$user = $action->fetch(PDO::FETCH_OBJ);

			$salt = $user->salt;
			$new_password = PasswordHasher::hashPassword($password, $salt);
			$old_password = $user->password;

			if ($new_password == $old_password) {
				array_push($this->problems, "Your new password must differ from your previous password.");
				return false;
			}

			$action = $this->connection->prepare("UPDATE users SET password=?, must_change_password=? WHERE id=?");
			$action->execute(array($new_password, 0, $user_id));
			
			return true;
		}

		function __construct() {
			$database_connection = new DatabaseConnection();
			$this->connection = $database_connection->connection;
			$this->problems = [];
		}

	}
?>