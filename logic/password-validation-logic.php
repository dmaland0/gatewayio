<?php
include_once $root_directory . '/../database-connection.php';
include_once $root_directory . '/../logic/password-complexity-configuration.php';
include_once $root_directory . '/../logic/password-hasher.php';

/**
* 
*/
class PasswordValidationLogic
{
	private $password_complexity;
	private $connection;
	
	function __construct()
	{
		$this->password_complexity = new PasswordComplexityConfiguration();

		$database_connection = new DatabaseConnection();
		$this->connection = $database_connection->connection;
	}

	public function validate($password, $password_change = null) {
		$problems = [];
		$matches = null;
		$password_complexity = $this->password_complexity;

		if (strlen($password) < $password_complexity->minimum_characters) {
			array_push($problems, "Your password does not have at least $password_complexity->minimum_characters characters.");
		}

		if (preg_match_all("/[A-Z]/", $password, $matches) < $password_complexity->minimum_capital_letters) {
			array_push($problems, "Your password does not have at least $password_complexity->minimum_capital_letters capital letter(s).");
		}

		if (preg_match_all("/[0-9]/", $password, $matches) < $password_complexity->minimum_numbers) {
			array_push($problems, "Your password does not have at least $password_complexity->minimum_numbers number(s).");
		}

		if (preg_match_all("/[^0-9A-Za-z\s]/", $password, $matches) < $password_complexity->minimum_special_characters) {
			array_push($problems, "Your password does not have at least $password_complexity->minimum_special_characters special character(s).");
		}

		if ($password_change != null) {

			if (isset($_COOKIE["authentication_token"])) {
				$find_user_with_token = $this->connection->prepare("SELECT * FROM users WHERE authentication_token=?");
				$find_user_with_token->execute(array($_COOKIE["authentication_token"]));
				$user = $find_user_with_token->fetch(PDO::FETCH_OBJ);

				$hashed_password = PasswordHasher::hashPassword($password, $user->salt);

				if ($hashed_password == $user->password) {
					array_push($problems, "Your new password must differ from your old password.");
				}

			} else {
				$problems = ["Your authentication token is invalid or missing. You must login again to get a new token."];
			}

		}

		return $problems;
	}
}
?>