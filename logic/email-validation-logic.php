<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];
include_once $root_directory . '/../database-connection.php';

	class EmailValidationLogic
	{
		
		function __construct()
		{}

		public function validate($email) {
			$result = [];

			if (preg_match("~.@.+\...~", $email) < 1) {
				array_push($result, "You don't seem to have entered a valid email.");
			}

			$database_connection = new DatabaseConnection();
			$connection = $database_connection->connection;

			$query = $connection->prepare("SELECT email FROM users WHERE email = ?");
			$query->execute(array($email));
			$result_object = $query->fetch(PDO::FETCH_OBJ);

			if (!empty($result_object)) {
				array_push($result, "A user with this email address is already registered.");
 			}

			return $result;
		}
	}
?>