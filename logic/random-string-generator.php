<?php

	class RandomStringGenerator {

		public static function generate($length) {
			$string = "";

			for ($i=0; $i < $length; $i++) { 
				$string = $string . chr(mt_rand(33,126));
			}

			return $string;
			
		}

		function __construct() {
			
		}

	}

?>
