<?php

	class AuthenticationResponses
	{
		public $successfulAuthentication;

		public function authenticationResponseHandler($authenticator_output) {
			$root_directory =  $_SERVER['DOCUMENT_ROOT'];
			
			if ($authenticator_output['success'] == true) {
				$this->successfulAuthentication = true;
			} else {
				$this->successfulAuthentication = false;
			}

			if ($authenticator_output['message'] == "Submitted Credential Mismatch") {
				include $root_directory . '/../partials/form-authentication-failed.php';
				return false;
			}

			if ($authenticator_output['message'] == "No Token") {
				include $root_directory . '/../partials/no-authentication-token.php';
				return false;
			}

			if ($authenticator_output['message'] == "Token Mismatch") {
				include $root_directory . '/../partials/token-authentication-failed.php';
				return false;
			}

			if ($authenticator_output['message'] == "IP Mismatch") {
				include $root_directory . '/../partials/authentication-ip-mismatch.php';
				return false;
			}

			if (strripos($authenticator_output['message'], "throttle") != false) {
				include $root_directory . '/../partials/authentication-throttled.php';
				return false;
			}
				
			if ($authenticator_output['user_must_change_password'] != 0) {
				include $root_directory . '/../partials/must-change-password.php';
				$this->successfulAuthentication = false;
				return false;
			}

		}
		
		function __construct() {
			$this->successfulAuthentication = false;
		}

	}
?>