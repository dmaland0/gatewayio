<?php

	class PasswordHasher
	{
		
		function __construct()
		{}

		public static function hashPassword($password, $salt) {
			
			return hash('sha512', $password . $salt);

		}
	}
?>