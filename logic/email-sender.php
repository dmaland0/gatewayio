<?php

	class EmailSender
	{
		
		function __construct()
		{}

		public static function send($email, $subject, $message) {
			
			$headers = 'From: gatewayio-noreply@example.test';

			$result = mail($email, $subject, $message, $headers);

			if (!$result) {
				return "The requested email could not be sent.";
			} else {
				return "The requested email was queued for delivery.";
			}

		}
	}
?>