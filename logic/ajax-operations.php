<?php
	class ajaxOperations {
		
		public static function readJSON() {
			$json_input = json_decode(file_get_contents('php://input'));
			return $json_input;
		}

		public static function respondWithJSON($data_for_response) {
			header('Content-type: application/json');
			echo json_encode($data_for_response);
		}

		public static function respondWithRaw($data_for_response) {
			header('Content-type: application/json');
			print_r($data_for_response);
		}

	}

	class response {
		public $success;
		public $message;
		public $data;

		public function __construct($success, $message, $data) {
			$this->success = $success;
			$this->message = $message;
			$this->data = $data;
		}
	}
?>