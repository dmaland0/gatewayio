<?php

	$root_directory = $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	
	class AdminRegistrationQuery {

		public $admin_can_be_registered = null;

		function __construct() {
			$database_connection = new DatabaseConnection();

			if ($database_connection->connection) {
				$query_text = "SELECT user_id FROM security_groups WHERE security_group = 'admin'";		
				$admins = $database_connection->connection->query($query_text)->fetchAll();

				$query_text = "SELECT * FROM admin_registration";
				$admin_registration_information = $database_connection->connection->query($query_text)->fetchAll();

				if (count($admins) == 0 && (count($admin_registration_information)) > 0) {
					$this->admin_can_be_registered = true;
				} else {
					$this->admin_can_be_registered = false;
				}

			}

		}

	}

?>
