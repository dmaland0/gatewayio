<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/password-hasher.php';
	include_once $root_directory . '/../logic/throttling-logic.php';

	class AuthenticationLogic {
		private $connection;
		private $authentication_status;
		//$authentication_status keys: ['success', 'message', 'user_ID', 'user_must_change_password'];
		private $token;

		public function setACookie($authentication_token) {
            $domain = $_SERVER['HTTP_HOST'];

            if (strpos($domain,'localhost') >= 0) {
                $domain = "";
            }

			setcookie("authentication_token",$authentication_token,0,"/",$domain,false,true);
			$this->token = $authentication_token;
		}

		private function makeSaveAndSetToken($user) {
				$last_token_time = date("U");
				$random = openssl_random_pseudo_bytes (20);
				$authentication_token = hash('sha256', (string) $last_token_time.$random);
				$ip = $_SERVER['REMOTE_ADDR'];

				$update_user = $this->connection->prepare("UPDATE users SET authentication_token=?, last_token_unix_time=?, last_login_ip=? WHERE id=?");
				$update_user->execute(array($authentication_token, $last_token_time, $ip, $user->id));

				$this->setACookie($authentication_token);

				return $authentication_token;
		}

		private function newAuthentication($email, $password) {
			//All logins trigger the login throttler.
			$throttler = new ThrottlingLogic();
			$throttler_output = $throttler->throttle('login');

			if ($throttler_output != "Proceed") {
				$this->setACookie("Form-Based Login Invalid");

				$this->authentication_status['success'] = false;
				$this->authentication_status['message'] = $throttler_output;
				return false;
			}

			$find_user = $this->connection->prepare("SELECT * FROM users WHERE email=?");
			$find_user->execute(array($email));
			$user = $find_user->fetch(PDO::FETCH_OBJ);

			if ($user) {
				$hashed_input = PasswordHasher::hashPassword($password , $user->salt);

				if ($hashed_input != $user->password) {
					$user = false;
				}
			}

			if (!$user) {
				$this->setACookie("Form-Based Login Invalid");

				$this->authentication_status['success'] = false;
				$this->authentication_status['message'] = "Submitted Credential Mismatch";
				return false;
			} else {
				//Successful logins reset the throttler.
				$throttler->throttle('reset_logins');

				$authentication_token = $this->makeSaveAndSetToken($user);

				$this->authentication_status['success'] = true;
				$this->authentication_status['message'] = "New login from submitted credentials.";
				$this->authentication_status['user_ID'] = $user->id;
				$this->authentication_status['user_must_change_password'] = $user->must_change_password;
				return true;
			}

		}

		private function authenticateWithToken($token) {
			$ip = $_SERVER['REMOTE_ADDR'];
			$find_user_with_token = $this->connection->prepare("SELECT * FROM users WHERE authentication_token=?");
			$find_user_with_token->execute(array($token));
			$user = $find_user_with_token->fetch(PDO::FETCH_OBJ);

			if(!$user) {
				$this->setACookie("Token-Based Login Invalid");

				$this->authentication_status['success'] = false;
				$this->authentication_status['message'] = "Token Mismatch";
				return false;
			} else {

				if ($user->last_login_ip != $ip) {
					$this->authentication_status['success'] = false;
					$this->authentication_status['message'] = "IP Mismatch";
					return false;
				}

				$id = $user->id;

				$this->authentication_status['success'] = true;
				$this->authentication_status['message'] = "Authentication via token.";
				$this->authentication_status['user_ID'] = $user->id;
				$this->authentication_status['user_must_change_password'] = $user->must_change_password;
				return true;
			}

		}

		public function authenticate($use_token) {
	
			if($use_token) {
				
				if (isset($_COOKIE["authentication_token"])) {
					$this->token = $_COOKIE["authentication_token"];
					$this->authenticateWithToken($_COOKIE["authentication_token"]);
				} else {
					$this->authentication_status['success'] = false;
					$this->authentication_status['message'] = "No Token";
				}
				
				return $this->authentication_status;
			} else {
				$this->newAuthentication($_POST["loginEmail"],$_POST["loginPassword"]);
				return $this->authentication_status;
			}

		}

		public function userIsInSecurityGroup($group) {
			$authentication = $this->authenticate(true);
			
			if ($authentication['success'] != true) {
				return false;
			}

			$token = $this->token;

			$query = $this->connection->prepare("SELECT id FROM users where authentication_token = ?");
			$query->execute(array($token));
			$user = $query->fetch(PDO::FETCH_OBJ);

			if ($user) {

				$query = $this->connection->prepare("SELECT security_group FROM security_groups WHERE user_id = ? AND security_group = ?");
				$query->execute(array($user->id, $group));
				$security_group = $query->fetch(PDO::FETCH_OBJ);

				if ($security_group) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		}

		function __construct() {
			$database_connection = new DatabaseConnection();
			$this->connection = $database_connection->connection;
			$this->authentication_status = ['success'=> null, 'message'=>null, 'user_ID'=>null, 'user_must_change_password'=>0];
		}
	}

?>
