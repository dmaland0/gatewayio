<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/admin-registration-query.php';
	include_once $root_directory . '/../logic/password-validation-logic.php';
	include_once $root_directory . '/../logic/email-validation-logic.php';
	include_once $root_directory . '/../logic/throttling-logic.php';
	include_once $root_directory . '/../logic/password-hasher.php';

	class RegistrationLogic {
		/*
		Class expects $_POST to contain:
		registerEmail
		registerPassword
		registerRepeatPassword
		*/

		public $problems = [];
		public $success = [false];
		public $registered_to_security_group = null;

		private function registerNewUser($option, $connection) {
			$salt = openssl_random_pseudo_bytes(40);
            $salt = hash('sha512', $salt);
			$hashed_password = PasswordHasher::hashPassword($_POST["registerPassword"],$salt);

			$new_user = $connection->prepare("INSERT INTO users (email, password, salt, must_change_password) VALUES (?,?,?,?)");

			if ($option == "admin") {
				$must_change_password = 1;
			} else {
				$must_change_password = 0;
			}

			$new_user->execute(array($_POST["registerEmail"], $hashed_password, $salt, $must_change_password));

			$find_user = $connection->prepare("SELECT id FROM users WHERE email = ?");
			$find_user->execute(array($_POST["registerEmail"]));
			$user = $find_user->fetch(PDO::FETCH_OBJ);

			$id = $user->id;

			$add_to_security = $connection->prepare("INSERT INTO security_groups (user_id, security_group) VALUES (?,?)");

			if($option == "admin") {
				$group = "admin";
			} else {
				$group = "user";
			}

			$add_to_security->execute(array($id, $group));
		}

		function __construct() {
			$registration_query = new AdminRegistrationQuery();
			$database_connection = new DatabaseConnection();
			$password_validation = new PasswordValidationLogic();
			$email_validation = new EmailValidationLogic();
			$throttling_logic = new ThrottlingLogic();

			$throttle_result = $throttling_logic->throttle('registration');

			if ($throttle_result != "Proceed") {
				$this->success = ["throttled",$throttle_result];
				return $this->success;
			}

			$connection = $database_connection->connection;

			if ($_POST["registerPassword"] != $_POST["registerRepeatPassword"]) {
				array_push($this->problems, "Your password and repeated password don't match.");
			}

			$validation_problems = $password_validation->validate($_POST["registerPassword"]);
			$email_validation_results = $email_validation->validate($_POST["registerEmail"]);
			
			if ($email_validation_results != null) {

				foreach ($email_validation_results as $result) {
					array_push($validation_problems, $result);
				}
				
			}

			foreach ($validation_problems as $validation_problem) {
				array_push($this->problems, $validation_problem);
			}

			if (count($this->problems) > 0) {
				return $this->success;
			}

			if ($registration_query->admin_can_be_registered) {
				$admin_registration_match = $connection->prepare("SELECT * FROM admin_registration WHERE expected_email = ? AND expected_password = ?");
				$admin_registration_match->execute(array($_POST["registerEmail"],$_POST["registerPassword"]));
				$match = $admin_registration_match->fetch(PDO::FETCH_OBJ);

				if (!empty($match)) {
					$this->registerNewUser("admin", $connection);
					$clear_admin_registration = $connection->query("DELETE FROM admin_registration");
					$this->success = ["success"];
					$this->registered_to_security_group = "\"Admin\"";
					return $this->success;
				} else {
					$this->success = ["bailout"];
					return $this->success;
				}

			} else {
				$this->registerNewUser("default", $connection);
				$this->success = ["success"];
				$this->registered_to_security_group = "\"User\"";
				return $this->success;
			}

		}
	}	

?>