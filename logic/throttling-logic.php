<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';

	class ThrottlingLogic {
		public $configuration;
		private $connection;

		private function getLoginAttempts($connection, $ip, $email) {
				$query = $connection->prepare("SELECT * FROM login_requests WHERE ip_address = ? OR email = ?");
				$query->execute(array($ip, $email));
				return $query->fetchAll(PDO::FETCH_OBJ);
			}

		private function getIPRegistrations($connection, $ip) {
			$query = $connection->prepare("SELECT * FROM registration_requests WHERE ip_address = ?");
			$query->execute(array($ip));
			//We use fetch, because we only need the first one.
			return $query->fetch(PDO::FETCH_OBJ);
		}

		private function getPasswordResetRequests($connection, $ip) {
				$query = $connection->prepare("SELECT * FROM password_reset_requests WHERE ip_address = ?");
				$query->execute(array($ip));
				//We use fetch, because we only need the first one.
				return $query->fetch(PDO::FETCH_OBJ);
			}

		public function throttle($what_to_throttle) {
			$ip = $_SERVER['REMOTE_ADDR'];
			$now = time();

			if ($what_to_throttle == 'registration') {
				$registration_requests = $this->getIPRegistrations($this->connection, $ip);

				if (!$registration_requests) {
					$action = $this->connection->prepare("INSERT INTO registration_requests (ip_address, registration_counter, php_time_of_last_registration) VALUES (?, ?, ?)");
					$action->execute(array($ip, 1, $now));

					$registration_requests = $this->getIPRegistrations($this->connection, $ip);
				} else {

					if ($registration_requests->registration_counter > $this->configuration->max_registrations_per_ip) {
						$unlock_time = $registration_requests->php_time_of_last_registration + (60*$this->configuration->registration_lockout_in_minutes);
						
						if ($now < $unlock_time) {
							$minutes_remaining = round(($unlock_time - $now) / 60);

							if ($minutes_remaining > 1) {
								$plural = "s";
							} else {
								$plural= "";
							}

							return "We won't process any further registration requests from you for about $minutes_remaining minute$plural.";
						} else {
							$registration_requests->registration_counter = 0;
						}

					}

					$action = $this->connection->prepare("UPDATE registration_requests SET registration_counter = ?, php_time_of_last_registration = ? WHERE ip_address = ?");
					$action->execute(array($registration_requests->registration_counter += 1, $now, $registration_requests->ip_address));

				}

				return "Proceed";
			}

			if ($what_to_throttle == 'login') {
				$email = $_POST["loginEmail"];
				$login_attempts = $this->getLoginAttempts($this->connection, $ip, $email);
				
				if (!$login_attempts) {
					$action = $this->connection->prepare("INSERT INTO login_requests (ip_address, email, login_counter, php_time_of_last_login) VALUES (?, ?, ?, ?)");
					$action->execute(array($ip, $email, 1, $now));

					$login_attempts = $this->getLoginAttempts($this->connection, $ip, $email);
				} else {
					//Login attempts can match IP addresses or emails within a record.
					//If one IP is banging away at a bunch of emails, the throttler will catch them.
					//If multiple IPs are trying an email, that will get caught as well.
					foreach ($login_attempts as $attempt) {
						
						if ($attempt->login_counter >= $this->configuration->max_login_requests) {
							$unlock_time = $attempt->php_time_of_last_login + (60*$this->configuration->login_lockout_in_minutes);

							if ($now < $unlock_time) {
								$minutes_remaining = round(($unlock_time - $now) / 60);

								if ($minutes_remaining > 1) {
									$plural = "s";
								} else {
									$plural= "";
								}

								return "Your logins are currently being throttled. We won't process any further login attempts from you for about $minutes_remaining minute$plural.";
							} else {
								$attempt->login_counter = 0;
							}

						}

						$action = $this->connection->prepare("UPDATE login_requests SET login_counter = ?, php_time_of_last_login = ? WHERE id = ?");
						$action->execute(array($attempt->login_counter += 1, $now, $attempt->id));

					}

				}

				return "Proceed";
			}

			if ($what_to_throttle == 'reset_logins') {
				$email = $_POST["loginEmail"];
				$login_attempts = $this->getLoginAttempts($this->connection, $ip, $email);
				
				if (!$login_attempts) {
					$action = $this->connection->prepare("INSERT INTO login_requests (ip_address, email, login_counter, php_time_of_last_login) VALUES (?, ?, ?, ?)");
					$action->execute(array($ip, $email, 1, $now));

					$login_attempts = $this->getLoginAttempts($this->connection, $ip, $email);
				}

				foreach ($login_attempts as $attempt) {
					$action = $this->connection->prepare("UPDATE login_requests SET login_counter = ?, php_time_of_last_login = ? WHERE id = ?");
					$action->execute(array($attempt->login_counter = 1, $now, $attempt->id));
				}

				return "Proceed";
				
			}

			if ($what_to_throttle == 'password-reset') {
				$reset_request = $this->getPasswordResetRequests($this->connection, $ip);

				if (!$reset_request) {
					$action = $this->connection->prepare("INSERT INTO password_reset_requests (ip_address, request_counter, php_time_of_last_request) VALUES (?, ?, ?)");
					$action->execute(array($ip, 1, $now));

					$reset_request = $this->getPasswordResetRequests($this->connection, $ip);
				} else {
					
					if ($reset_request->request_counter >= $this->configuration->max_password_reset_requests) {
						$unlock_time = $reset_request->php_time_of_last_request + (60*$this->configuration->password_reset_lockout_in_minutes);

						if ($now < $unlock_time) {
							$minutes_remaining = round(($unlock_time - $now) / 60);

							if ($minutes_remaining > 1) {
								$plural = "s";
							} else {
								$plural= "";
							}

							return "Your password reset requests are currently being throttled. We won't process any further reset requests from you for about $minutes_remaining minute$plural.";

						} else {
							$reset_request->request_counter = 0;
						}
					}

					$action = $this->connection->prepare("UPDATE password_reset_requests SET request_counter = ?, php_time_of_last_request = ? WHERE id = ?");
					$action->execute(array($reset_request->request_counter += 1, $now, $reset_request->id));

				}

				return "Proceed";
			}

		}
		
		function __construct() {
			$database_connection = new DatabaseConnection();
			$this->connection = $database_connection->connection;

			$configuration_query = $this->connection->query("SELECT * FROM throttling_configuration");
			$this->configuration = $configuration_query->fetch(PDO::FETCH_OBJ);
		}
	}

?>
