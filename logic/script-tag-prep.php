<?php
	function scriptTagPrep() {
		$secure = (preg_match('/HTTPS/',$_SERVER['SERVER_PROTOCOL']));
                
            if ($secure){
                $protocol = "https";
            } else {
                $protocol = "http";
            }
                
            $host = $_SERVER['HTTP_HOST'];

            $info = new stdClass();

            $info->protocol = $protocol;
            $info->host = $host;

            return $info;
	}
?>