<div id="dashboard-box" class="roundBox neutralBox">
	<h1>
	<span id="nameData">
	<?php 

		if($user_details[0]->name) { 
			echo $user_details[0]->name . ", ";
		}
	?>
	</span>
		Welcome To Your Dashboard
	</h1>
	
	<p>
		<strong>Your email on this system is:</strong>
		<br>
		<span id="emailData">
		<?php 
			echo $user_details[0]->email;
		?>
		</span>
	</p>
	<p>
		<strong>You are in the following security groups:</strong>
		<?php
			foreach ($user_details as $key => $value) {
				echo "<br>".$user_details[$key]->security_group;
			}
		?>
	</p>
	<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('nameChangeWrapper')">Change Your Name</button>
	<div id="nameChangeWrapper" class="collapsible hideable">
		<?php
			include $root_directory . '/../partials/name-change-form.php';
		?>
	</div>
	<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('emailChangeWrapper')">Change Your Email</button>
	<div id="emailChangeWrapper" class="collapsible hideable">
		<?php
			include $root_directory . '/../partials/email-change-form.php';
		?>
	</div>
	<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('changePasswordWrapper')">Change Your Password</button>
	<div id="changePasswordWrapper" class="collapsible hideable">
		<?php
			include $root_directory . '/../partials/password-change-form.php';
		?>
	</div>
		<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('deleteAccountWrapper')">Delete Your Account</button>
	<div id="deleteAccountWrapper" class="collapsible hideable">
		<?php
			include $root_directory . '/../partials/dashboard/delete-account-form.php';
		?>
	</div>
	<form action="login.php" method="post">
		<input type="hidden" id="user_id" name="user_id" value=<?php $value = $user_authenticated['user_ID']; echo "\"$value\""?>>
		<input type="submit" class="bigSubmit" value="Log Out">
	</form>
	<div class="clear">
	</div>
</div>
<script src="/scripts/dashboard/common-dashboard-controller.js"></script>
<script src="/scripts/filters.js"></script>