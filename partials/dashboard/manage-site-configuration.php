<div id="manage-site-config-box" class="roundBox neutralBox widerBox hideable">
	<h2>Manage The Site Configuration</h2>
	<div id="configForm" class="foundationForm adminForm">
		<h5>Password Complexity</h5>
		<label>Minimum Total Length</label>
		<br>
		<input type="number" id="minimumCharacters">
		<br>
		<label>Minimum Capital Letters</label>
		<br>
		<input type="number" id="minimumCapitalLetters">
		<br>
		<label>Minimum Numbers</label>
		<br>
		<input type="number" id="minimumNumbers">
		<br>
		<label>Minimum Special Characters</label>
		<br>
		<input type="number" id="minimumSpecialCharacters">
		<br>
		<button class="customButton" onclick="siteConfigController.submitPasswordConfig()">Update Password Configuration</button>
		<hr>
		<h5>Throttling</h5>
		<div class="roundBox widerBox">
			<label>Maximum Login Requests</label>
			<br>
			<input type="number" id="maxLogins">
			<br>
			<label>Login Lockout In Minutes</label>
			<br>
			<input type="number" id="loginLockout">
		</div>
		<div class="roundBox widerBox">
			<label>Maximum Registrations Per IP Address</label>
			<br>
			<input type="number" id="maxRegistrations">
			<br>
			<label>Registration Lockout In Minutes</label>
			<br>
			<input type="number" id="registrationLockout">
		</div>
		<div class="roundBox widerBox">
			<label>Maximum Password Reset Requests</label>
			<br>
			<input type="number" id="maxPasswordResets">
			<br>
			<label>Password Reset Lockout In Minutes</label>
			<input type="number" id="resetLockout">
		</div>
		<button class="customButton" onclick="siteConfigController.submitThrottlingConfig()">Update Throttling Configuration</button>
	</div>
	<div id="configManagementError" class="ajaxErrorMessage"></div>
</div>
<script src="/scripts/dashboard/site-config-controller.js"></script>