<div id="admin-dashboard-box" class="roundBox neutralBox">
	<h1>Admin Dashboard</h1>
	<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('manage-a-user-box')">Manage A User</button>
	<button class="dashboardButton" onclick="commonDashboardController.toggleDisplayOf('manage-site-config-box')">Manage Site Configuration</button>
	<?php
		include $root_directory . '/../partials/dashboard/manage-a-user.php';
		include $root_directory . '/../partials/dashboard/manage-site-configuration.php'
	?>
</div>