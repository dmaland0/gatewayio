<div id="manage-a-user-view" class="roundBox neutralBox widerBox">
		<h2>User View</h2>
		<div class="clear"></div>

		<div class="userViewColumn">
			<h3>ID</h3>
			<p id="userViewID"></p>
		</div>

		<div class="userViewColumn">
			<h3>Name</h3>
			<p id="userViewName"></p>
		</div>

		<div class="userViewColumn">
			<h3>Email</h3>
			<p id="userViewEmail"></p>
		</div>

		<div class="userViewColumn">
			<h3>Must Change Password?</h3>
			<button id="userViewMustChangePassword" class="largerButton" onclick="userManagerController.toggleMustChangePassword()"></button>
		</div>

		<div class="clear"></div>

		<div class="userViewColumn">
			<h3>Set Password To:</h3>
			<input id="userViewSetPasswordTo" oninput="userManagerController.validatePassword()">
			<br><br>
			<button id="userViewSetPassword" class="largerButton" onclick="userManagerController.setUserPassword()">Set Password</button>
			<div id="userViewSetPasswordProblems"></div>
		</div>

		<div class="userViewColumn">
			<h3>Security Groups:</h3>
			<p id="userViewGroups"></p>
			<h3>Select An Available Group:</h3>
			<select id="userViewSelectGroup">
				
			</select>
			<h3>Specify A New Group:</h3>
			<input id="userViewNewGroup" oninput="userManagerController.enableUserViewAddToGroup()">
			<br><br>
			<button id="userViewAddToGroup" class="largerButton" onclick="userManagerController.addSecurityGroup()">Add User To Selected Group</button>
		</div>

		<div class="userViewColumn">
			<h3>Delete This User</h3>
			<h4 class="warning">Warning: This can not be undone!</h4>
			<p>Click the buttons as they enable to unlock the delete button.</p>
			<button id="deleteUnlock1" class="largerButton" onclick="userManagerController.deleteUnlock()" disabled="disabled">1</button>
			<button id="deleteUnlock2" class="largerButton" onclick="userManagerController.deleteUnlock()" disabled="disabled">2</button>
			<button id="deleteUnlock3" class="largerButton" onclick="userManagerController.deleteUnlock()" disabled="disabled">3</button>
			<button id="deleteUnlock4" class="largerButton" onclick="userManagerController.deleteUnlock()" disabled="disabled">4</button>
			<button id="deleteUnlock5" class="largerButton" onclick="userManagerController.deleteUnlock()" disabled="disabled">5</button>
			<br><br>
			<button id="deleteUser" class="largerButton" onclick="userManagerController.deleteUser()" disabled="disabled">Delete User</button>
		</div>

		<div class="clear"></div>

	<div class="ajaxErrorMessage" id="userActionError">
		
	</div>

	<button class="bigSubmit" onclick="userManagerController.dismiss()">Dismiss</button>

	<div class="clear"></div>

</div>
<script src="/scripts/dashboard/user-manager-controller.js"></script>