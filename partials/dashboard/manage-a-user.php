<div id="manage-a-user-box" class="roundBox neutralBox widerBox hideable">
		<h2>Manage A User</h2>
		<div id="userSearchForm" class="foundationForm adminForm">
			<label>Search by User ID, Name, or Email:</label>
			<br>
			<input id="userSearchTerm" oninput="userSearchController.search()">
			<div id="userSearchResultsBox">
				<label>Select A Result:</label>
				<table id="userSearchResultsTable">
					<thead>
						<th>ID</th><th>Name</th><th>Email</th>
					</thead>
					<tbody id="userSearchData">
						
					</tbody>
				</table>
			</div>
			<div class="ajaxErrorMessage" id="userSearchError">
				
			</div>
		</div>
	<?php
		include $root_directory . '/../partials/dashboard/manage-a-user-view.php';
	?>
</div>
<script src="/scripts/dashboard/user-search-controller.js"></script>