<form name="deleteAccountForm" id="deleteAccountForm" class="foundationForm" action="delete-account.php" method="post">
  	<label>Account deletions are permanent, and can not be undone!</label>
	<br><br>
	<label>Enter Your Email</label>
	<br>
	<input name="loginEmail">
	<br>
	<label>Enter Your Password</label>
	<br>
	<input type="password" name="loginPassword">
	<br>
	<input type="submit" value="Confirm..." id="deleteAccountButton" class="customButton">
</form>