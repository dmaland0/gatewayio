<div id="registrationSuccess" class="roundBox successBox">
	<h1>Your registration has been accepted.</h1>
	<h2>You can now <a href="login.php">login</a> normally.</h2>
	<p>You were registered in the security group <?php echo $logic->registered_to_security_group?>.</p>
</div>