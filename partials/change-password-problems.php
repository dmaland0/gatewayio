<div id="passwordChangeProblems" class="roundBox errorBox">
	<h1>Sorry, but your password change could not be processed.</h1>
	<h2>The following problems were detected:</h2>
	
	<?php
		
		foreach ($changePasswordLogic->problems as $key => $value) {
			echo "<p>$value</p>";
		}

	?>

	<h2>Please go back and try again.</h2>

</div>