<div id="throttledAuthentication" class="roundBox errorBox">
	<h1>We couldn't process your authentication attempt.</h1>
	<p><?php echo $authenticator_output['message']?></p>
</div>
