<form name="passwordChangeForm" id="passwordChangeForm" class="foundationForm" action="update-password.php" method="post">
	<label>New Password</label>
	<br>
	<input type="password" name="newPassword" id="newPassword" oninput="newPasswordFormValidator.validate()">
	<br>
	<div class="registrationFormError" id="passwordRegistrationError"></div>
	<label>Repeat New Password</label>
	<br>
	<input type="password" name="repeatNewPassword" id="repeatNewPassword" oninput="newPasswordFormValidator.validate()">
	<br>
	<div class="registrationFormError" id="repeatPasswordRegistrationError"></div>
	<input type="submit" value="Submit" id="newPasswordButton" class="customButton">
</form>
<script src="/scripts/new-password-form-validator.js"></script>