<div id="adminRegister">
	There do not appear to be any registered admins in the database.<br><br>If the database has an expected email and password provided in the table "admin_registration," registering with those credentials will create the Admin/ Root user for this application.
</div>