<div id="deletionSuccess" class="roundBox successBox">
	<h1>Your account was deleted.</h1>
	<h2>You can return to the <a href="index.php">homepage</a> now.</h2>
</div>