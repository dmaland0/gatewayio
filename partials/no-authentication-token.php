<div id="failedTokenAuthentication" class="roundBox errorBox">
	<h1>Sorry, your authentication attempt failed.</h1>
	<p>We weren't able to authenticate you as a valid user on this system, because your security token cookie seems to have been deleted, blocked, or never set in the first place.</p>
	<p>If you are blocking cookies to this site, please remove the block.</p>
	<p>Please <a href="login.php">login</a> again, and a new token will be created for you.</p>
</div>
