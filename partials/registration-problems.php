<div id="registrationProblems" class="roundBox errorBox">
	<h1>Sorry, but your registration could not be processed.</h1>
	<h2>The following problems were detected:</h2>
	
	<?php
		foreach ($logic->problems as $key => $value) {

			if ($value == "A user with this email address is already registered.") {
				echo "<p style=\"color: red; font-weight: 900;\">$value</p>";
			}
			else {
				echo "<p>$value</p>";}
			}

	?>

	<h2>Please go <a href="login.php">back</a> and try again.</h2>

</div>