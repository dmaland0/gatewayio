<div id="failedTokenAuthentication" class="roundBox errorBox">
	<h1>Sorry, your authentication attempt failed.</h1>
	<p>We weren't able to authenticate you as a valid user on this system based on the security token provided.</p>
	<p>Please <a href="login.php">login</a> again, and a new token will be created for you.</p>
</div>
