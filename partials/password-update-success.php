<div id="passwordChangeSuccess" class="roundBox successBox">
	<h1>Your password has been changed.</h1>
	<h2>Please <a href="login.php">login</a> to get a new security token. (This is recommended, but not strictly required.)</h2>
</div>