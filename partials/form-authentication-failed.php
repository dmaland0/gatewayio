<div id="failedFormAuthentication" class="roundBox errorBox">
	<h1>Sorry, your authentication attempt failed.</h1>
	<p>We weren't able to authenticate you as a valid user on this system based on the username and password you entered.</p>
	<p>Please <a href="login.php">try</a> again.</p>
</div>
