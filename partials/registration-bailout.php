<div id="registrationBailout" class="roundBox warningBox">
	<h1>Sorry, but your registration wasn't accepted.</h1>
	<h2>This usually happens when admin_registration is active, but your registration attempt didn't have the right email or password.</h2>
	<p>Please go <a href="login.php">back</a> and try again.</p>
</div>