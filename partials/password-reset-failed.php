<div id="passwordResetFailed" class="roundBox errorBox">
	<h1>Sorry, but your password reset could not be processed.</h1>
	
	<p><?php echo $result['message'];?></p>

	<?php
		if (!strripos($result['message'], "throttled")) {
			echo "<h2>Please go <a href=\"password-reset-form.php\">back</a> and try again.</h2>";
		}
	?>

</div>