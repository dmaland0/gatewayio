<div id="authenticationIPMismatch" class="roundBox warningBox">
	<h1>There's an issue with your authentication.</h1>
	<p>Your authentication token seems correct, but it was submitted from an IP address that doesn't match the address it was created for.</p>
	<p>Please <a href="login.php">login</a> again, and a new token will be created for you.</p>
</div>
