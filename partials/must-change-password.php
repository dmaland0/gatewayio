<div id="mustChangePassword" class="roundBox warningBox">
	<h1>You must change your password.</h1>
	<p>In order to continue using your account securely, you need to create a new password.</p>
	<?php
		$root_directory =  $_SERVER['DOCUMENT_ROOT'];
		include $root_directory . '/../partials/password-change-form.php';
	?>
</div>
