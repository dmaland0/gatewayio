<div id="emailChangeForm" class="foundationForm">
	<label>New Email: </label>
	<br>
	<input id="email" oninput="updateEmailFormController.validate()">
	<div class="registrationFormError" id="emailChangeError"></div>
	<input id="emailSubmit" type="submit" value="Submit" disabled="disabled" class="customButton" onclick="updateEmailFormController.submit()">
	<div class="registrationFormError" id="emailChangeSubmitError"></div>
</div>
<script src="/scripts/dashboard/update-email-form-controller.js"></script>