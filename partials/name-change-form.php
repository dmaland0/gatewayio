<div id="nameChangeForm" class="foundationForm">
	<label>New Name: </label>
	<br>
	<input id="newName" oninput="updateNameFormController.validate()">
	<div class="registrationFormError" id="nameChangeError"></div>
	<input id="nameSubmit" type="submit" value="Submit" disabled="disabled" class="customButton" onclick="updateNameFormController.submit()">
	<div class="registrationFormError" id="nameChangeSubmitError"></div>
</div>
<script src="/scripts/dashboard/update-name-form-controller.js"></script>