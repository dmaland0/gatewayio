<div id="passwordResetSucceeded" class="roundBox successBox">
	<h1>Your password reset request was successfully processed.</h1>
	
	<p><?php echo $result['message'];?></p>

	<h2>Please <a href="login.php">login</a> with your new password.</h2>

</div>