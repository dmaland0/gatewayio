<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/authentication-logic.php';
	include_once $root_directory . '/../logic/authentication-responses.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$authenticator = new AuthenticationLogic();
	//If nothing was POSTed to the server, then there's no login form data to use for authentication, and a token is required.
	$useToken = empty($_POST);
	$user_authenticated = $authenticator->authenticate($useToken);
	$admin = false;
?>
<!doctype html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>
		<div class="content">
			<?php 
				$authentication_responses = new AuthenticationResponses();
				$authentication_responses->authenticationResponseHandler($user_authenticated);

				if ($authentication_responses->successfulAuthentication) {
					$user_details = [""];
					$query = $connection->prepare("SELECT * FROM users JOIN security_groups ON users.id = security_groups.user_id WHERE users.id = ?");
					$query->execute(array($user_authenticated['user_ID']));
					$user_details  = $query->fetchAll(PDO::FETCH_OBJ);
					
					include $root_directory . '/../partials/dashboard/common-dashboard.php';

					foreach ($user_details as $key => $value) {
						
						if ($user_details[$key]->security_group == "admin") {
							$admin = true;
							include_once $root_directory . '/../partials/dashboard/admin-dashboard.php';
						}

					}
				}
			?>
		</div>
		<?php
			if ($admin) {
				echo "<img src=\"/images/spinner.svg\" class=\"spinner\" id=\"globalSpinner\">";
				echo "<script src=\"/scripts/dashboard/spinner-injector.js\"></script>";
			}
		?>
	</body>
</html>
