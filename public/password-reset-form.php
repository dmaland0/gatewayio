<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
?>
<!doctype html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>
		<div class="content">
			<div id="resetPassword" class="roundBox warningBox">
				<h1>Reset your password.</h1>
					<p>The system will try to send you an email with a temporary password. If the email doesn't seem to have arrived after a few minutes, please check your spam folder before trying again.</p>
					<form name="passwordResetForm" id="passwordResetForm" class="foundationForm" action="password-reset.php" method="post">
						<label>Email</label>
						<br>
						<input name="email" id="email" oninput="resetPasswordFormValidator.validate()">
						<br>
						<div class="registrationFormError" id="emailError"></div>
						<br>
						<input type="submit" value="Submit" id="resetPasswordButton" class="customButton">
					</form>
					
					<script src="/scripts/reset-password-form-validator.js"></script>
					
			</div>
		</div>
	</body>
</html>