<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {
			$action = $connection->prepare("UPDATE throttling_configuration SET max_login_requests = ?, login_lockout_in_minutes = ?, max_registrations_per_ip = ?, registration_lockout_in_minutes = ?, max_password_reset_requests = ?, password_reset_lockout_in_minutes = ?");
			$action->execute(array($input->maxLogins, $input->loginLockout, $input->maxRegistrations, $input->registrationLockout, $input->maxPasswordResets, $input->resetLockout));

			$response = new response(true, "Throttling configuration updated.", null);
		} else {
			$response = new response(false, "Insufficient permission level for throttling configuration update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>