<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';
	include_once $root_directory . '/../logic/password-change-logic.php';

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {
			$passwordChangeLogic = new PasswordChangeLogic();
			
			if ($passwordChangeLogic->changePassword($input->user, $input->password, $input->password)) {
				$response = new response(true, "User password was changed.", $input->user);
			} else {
				$response = new response(false, "User password update failed.", $passwordChangeLogic->problems);
			}

			
		} else {
			$response = new response(false, "Insufficient permission level for user update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>