<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {

			if ($input->searchTerm != "") {
				$search_term = $input->searchTerm . "%";

				$query = $connection->prepare("SELECT id, name, email FROM users WHERE name LIKE ? OR email LIKE ? OR id LIKE ?");
				$query->execute(array($search_term, $search_term, $search_term));
				$users = $query->fetchAll(PDO::FETCH_OBJ);

				$response = new response(true, "The database was searched.", $users);
			} else {
				$users = [];
				$response = new response(true, "No search term", $users);
			}


		} else {
			$response = new response(false, "Insufficient permission level for user search.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>