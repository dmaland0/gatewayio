<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	function removeGroup($input, $connection) {
		$action = $connection->prepare("DELETE FROM security_groups WHERE id = ?");
		$action->execute(array($input->selectedGroup->id));

		return new response(true, "User removed from security group", $input->selectedGroup->user_id);
	}

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {

			$authenticated_user = $authentication->authenticate(true);

			if ($authenticated_user['user_ID'] == $input->selectedGroup->user_id) {

				if ($input->selectedGroup->security_group == "admin") {
					$response = new response(false, "You can not remove yourself from the admin group.", null);
				} else {
					$response = removeGroup($input, $connection);
				}

			} else {
				$response = removeGroup($input, $connection);
			}
			
		} else {
			$response = new response(false, "Insufficient permission level for user update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>