<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$query = $connection->prepare("SELECT id FROM users WHERE authentication_token = ?");
		$query->execute(array($_COOKIE["authentication_token"]));
		$user = $query->fetch(PDO::FETCH_OBJ);

		if ($user) {

			try {
				$action = $connection->prepare("UPDATE users SET email = ? WHERE id = ?");
				$action->execute(array($input->email, $user->id));

				$query = $connection->prepare("SELECT email FROM users WHERE email = ?");
				$query->execute(array($input->email));
				$user = $query->fetch(PDO::FETCH_OBJ);

				$response = new response(true, "The email was updated", $user->email);

			} catch (PDOException $e) {
				$response = new response(false, "Please try again. The database couldn't be updated.", null);
			}
			

		} else {
			$response = new response(false, "No user found with the given authentication token.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>