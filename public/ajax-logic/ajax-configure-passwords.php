<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {
			$action = $connection->prepare("UPDATE password_complexity_configuration SET minimum_characters = ?, minimum_capital_letters = ?, minimum_numbers = ?, minimum_special_characters = ?");
			$action->execute(array($input->minimumCharacters, $input->minimumCapitals, $input->minimumNumbers, $input->minimumSpecialCharacters));

			$response = new response(true, "Password complexity configuration updated.", null);
		} else {
			$response = new response(false, "Insufficient permission level for password complexity configuration update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>