<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/password-validation-logic.php';
	include_once $root_directory . '/../logic/ajax-operations.php';

	$input = ajaxOperations::readJSON();
	$problems = [];

	$password_validation = new PasswordValidationLogic();

	if ($input->passwordChange) {
		$password_validation_problems = $password_validation->validate($input->newPassword, $input->passwordChange);
	} else {
		$password_validation_problems = $password_validation->validate($input->newPassword);
	}
	

	if (count($password_validation_problems) > 0) {
		$problems["passwordInsufficientlyComplex"] = $password_validation_problems;
	}

	if ($input->newPassword != $input->repeatNewPassword) {
		$problems["passwordsUnmatched"] = true; 
	}

	ajaxOperations::respondWithJSON($problems);
?>