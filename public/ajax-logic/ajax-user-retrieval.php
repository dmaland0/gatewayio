<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {

			$query = $connection->prepare("SELECT id, name, email, must_change_password FROM users WHERE id = ?");
			$query->execute(array($input->user_id));
			$user = $query->fetch(PDO::FETCH_OBJ);

			$query = $connection->prepare("SELECT * FROM security_groups WHERE user_id = ?");
			$query->execute(array($input->user_id));
			$groups = $query->fetchAll(PDO::FETCH_OBJ);

			$query = $connection->prepare("SELECT DISTINCT security_group FROM security_groups");
			$query->execute();
			$possible_groups = $query->fetchAll(PDO::FETCH_OBJ);

			class compositeUser {
				public $user;
				public $groups;
				public $possible_groups;
			}

			$composite_user = new compositeUser();
			$composite_user->user = $user;
			$composite_user->groups = $groups;
			$composite_user->possible_groups = $possible_groups;

			$response = new response(true, "User retrieved.", $composite_user);
		} else {
			$response = new response(false, "Insufficient permission level for user retrieval.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>