<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];
include_once $root_directory . '/../logic/password-validation-logic.php';
include_once $root_directory . '/../logic/email-validation-logic.php';
include_once $root_directory . '/../logic/ajax-operations.php';

$input = ajaxOperations::readJSON();
$problems = [];

$email_validation = new EmailValidationLogic();
$email_validation_result = $email_validation->validate($input->email);

if (count($email_validation_result) > 0) {
	$problems["emailProblems"] = $email_validation_result;
}
	
$password_validation = new PasswordValidationLogic($input->password);
$password_validation_problems = $password_validation->validate($input->password);

if (count($password_validation_problems) > 0) {
	$problems["passwordInsufficientlyComplex"] = $password_validation_problems;
}

if ($input->password != $input->repeatPassword) {
	$problems["passwordsUnmatched"] = true; 
}

ajaxOperations::respondWithJSON($problems);
?>