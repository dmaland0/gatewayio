<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {

			$query = $connection->prepare("SELECT must_change_password FROM users WHERE id = ?");
			$query->execute(array($input->user));
			$user = $query->fetch(PDO::FETCH_OBJ);

			if ($user->must_change_password == 1) {
				$user->must_change_password = 0;
			} else {
				$user->must_change_password = 1;
			}

			$action = $connection->prepare("UPDATE users SET must_change_password = ? WHERE id= ?");
			$action->execute(array($user->must_change_password, $input->user));

			$response = new response(true, "Toggled must_change_password.", $input->user);
		} else {
			$response = new response(false, "Insufficient permission level for user update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>