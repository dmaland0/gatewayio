<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {
			$user = $authentication->authenticate(true);

			if ($user['user_ID'] == $input->userID) {
				$response = new response(false, "Sorry, but administrators can not delete themselves this way.", $input->userID);
			} else {
				$action = $connection->prepare("DELETE FROM users WHERE id = ?");
				$action->execute(array($input->userID));

				$response = new response(true, "User deleted.", null);
			}
			
			
		} else {
			$response = new response(false, "Insufficient permission level for user update.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>