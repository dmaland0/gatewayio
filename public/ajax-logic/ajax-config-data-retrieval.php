<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/ajax-operations.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$connect = new DatabaseConnection();
	$connection = $connect->connection;

	$input = ajaxOperations::readJSON();

	if (isset($_COOKIE["authentication_token"])) {

		$authentication = new AuthenticationLogic();

		if ($authentication->userIsInSecurityGroup("admin")) {
			$query = $connection->prepare("SELECT * FROM password_complexity_configuration");
			$query->execute();
			$password_config = $query->fetch(PDO::FETCH_OBJ);

			$query = $connection->prepare("SELECT * FROM throttling_configuration");
			$query->execute();
			$throttling_config = $query->fetch(PDO::FETCH_OBJ);

			class systemConfig {
				public $password_config;
				public $throttling_config;
			}

			$configuration = new systemConfig();
			$configuration->password_config = $password_config;
			$configuration->throttling_config = $throttling_config;

			$response = new response(true, "Configuration retrieved.", $configuration);
		} else {
			$response = new response(false, "Insufficient permission level for configuration retrieval.", null);
		}

	} else {
		$response = new response(false, "No authentication token transmitted.", null);
	}

	ajaxOperations::respondWithJSON($response);
?>