var updateEmailFormController = {
	newEmail: document.getElementById('email'),

	emailChangeError: document.getElementById('emailChangeError'),

	emailChangeSubmitError: document.getElementById('emailChangeSubmitError'),

	submitButton: document.getElementById('emailSubmit'),

	emailData: document.getElementById('emailData'),

	validate: function() {
		clearTimeout(window.lastTimer);

		window.lastTimer = setTimeout(function() {
			var request = new XMLHttpRequest();

			request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);
					
					if (response.emailProblems) {
						
						updateEmailFormController.emailChangeError.innerHTML = "<p>" + response.emailProblems[0] + "</p>";
						updateEmailFormController.submitButton.disabled = "disabled";
						
					} else {
						updateEmailFormController.emailChangeError.innerHTML = "";
						updateEmailFormController.submitButton.disabled = "";
					}
				}
			};

			request.open("POST", document.location.origin + "/ajax-logic/ajax-email-validation.php", true);
			request.send(JSON.stringify({email: email.value}));

		}, 750);
	},

	submit: function() {
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);
					
					if (response.success == false) {

						if (response.message.match("token")) {
							updateEmailFormController.emailChangeSubmitError.innerHTML = "<p>" + response.message + " Log in again to get a new token." + "</p>";
						} else {
							updateEmailFormController.emailChangeSubmitError.innerHTML = "<p>" + response.message + "</p>";
						}

					} else {
						updateEmailFormController.emailData.innerHTML = response.data;
						commonDashboardController.toggleDisplayOf('emailChangeWrapper');
					}
				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-email-update.php", true);
		request.send(JSON.stringify({email: updateEmailFormController.newEmail.value}));
	},
	
};