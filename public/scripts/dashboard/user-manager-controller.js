var userManagerController = {

	manageAUserView: document.getElementById('manage-a-user-view'),

	userViewID: document.getElementById('userViewID'),

	userViewName: document.getElementById('userViewName'),

	userViewEmail: document.getElementById('userViewEmail'),

	userViewMustChangePassword: document.getElementById('userViewMustChangePassword'),

	userViewGroups: document.getElementById('userViewGroups'),

	userActionError: document.getElementById('userActionError'),

	userViewSetPassword: document.getElementById('userViewSetPassword'),

	userViewSetPasswordTo: document.getElementById('userViewSetPasswordTo'),

	userViewSetPasswordProblems: document.getElementById('userViewSetPasswordProblems'),

	userViewAddToGroup: document.getElementById('userViewAddToGroup'),

	userViewSelectGroup: document.getElementById('userViewSelectGroup'),

	userViewNewGroup: document.getElementById('userViewNewGroup'),

	retrievedUser: null,

	deleteUnlockClicks: 0,

	updateDisplay: function() {
		var thisUser = userManagerController.retrievedUser;

		userManagerController.userActionError.innerHTML = "";
		userManagerController.userActionError.style.visibility = "hidden";

		userManagerController.userViewID.innerHTML = thisUser.user.id;
		userManagerController.userViewEmail.innerHTML = thisUser.user.email;
		userManagerController.userViewName.innerHTML = thisUser.user.name;
		userManagerController.userViewMustChangePassword.innerHTML = filters.booleanFilter(thisUser.user.must_change_password);

		userManagerController.userViewGroups.innerHTML = "";
		for (let group in thisUser.groups) {
			var imgString = '<img id="groupRemove-' + group + '" src="' + document.location.origin + "/images/remove-icon.svg" + '">';
			userManagerController.userViewGroups.innerHTML = userViewGroups.innerHTML + thisUser.groups[group].security_group + imgString + '<div class="clear"></div>';
		}

		var groups = userManagerController.userViewGroups.getElementsByTagName('img');

		for (var i = 0; i < groups.length; i++) {
			groups[i].setAttribute("onclick", "userManagerController.deleteGroup(" + i + ")");
		}

		if (groups.length == 1) {
			groups[0].className="disabled";
			groups[0].setAttribute("onclick", "");
		}


		userManagerController.userViewSelectGroup.innerHTML = "";
		for (let possibleGroup in thisUser.possible_groups) {

			var found = false;
			
			for (let group in thisUser.groups) {
				if (thisUser.possible_groups[possibleGroup].security_group == thisUser.groups[group].security_group) {
					found = true;
				}
			}

			if (!found) {
				userManagerController.userViewSelectGroup.innerHTML = userManagerController.userViewSelectGroup.innerHTML + "<option>" + thisUser.possible_groups[possibleGroup].security_group + "</option>";
			}
		}

		userManagerController.userViewNewGroup.value = "";

		if (userManagerController.userViewSelectGroup.value == "" && userManagerController.userViewSelectGroup.innerHTML == "") {
			userManagerController.userViewAddToGroup.disabled = "disabled";
		} else {
			userManagerController.userViewAddToGroup.disabled = "";
		}

		document.getElementById('deleteUser').disabled = "disabled";
		userManagerController.deleteUnlock();
		userManagerController.deleteUnlockClicks = 0;
	},

	show: function(userID) {
		userManagerController.manageAUserView.style.display = "block";

		var request = new XMLHttpRequest();
		if (userManagerController.userSpinner) {
			userManagerController.userSpinner.style.opacity = 1;
		}
		
		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					
					var response = JSON.parse(request.response);
					
					userManagerController.userViewSetPassword.disabled = "disabled";

					if (response.success) {
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.retrievedUser = response.data;
						userManagerController.updateDisplay();
						window.setTimeout(function(){userManagerController.userSpinner.style.opacity = 0;},2000);

					} else {
						userManagerController.userActionError.innerHTML = response.message;
						userManagerController.userActionError.style.visibility = "visible";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-user-retrieval.php", true);
		request.send(JSON.stringify({user_id: userID}));
	},

	dismiss: function() {
		userManagerController.userActionError.innerHTML = "";
		userManagerController.userViewID.innerHTML = "";
		userManagerController.userViewEmail.innerHTML = "";
		userManagerController.userViewName.innerHTML = "";
		userManagerController.userViewMustChangePassword.innerHTML = "";
		userManagerController.userViewSelectGroup.innerHTML = "";
		userManagerController.userViewGroups.innerHTML = "";
		userManagerController.manageAUserView.style.display = "none";

		userSearchController.unsetHighlight();
	},

	toggleMustChangePassword: function() {
		userManagerController.userSpinner.style.opacity = 1;
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.show(response.data);

					} else {
						userManagerController.userActionError.innerHTML = response.message;
						userManagerController.userActionError.style.visibility = "visible";
					}
				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-toggle-must-change-password.php", true);
		request.send(JSON.stringify({user: userManagerController.retrievedUser.user.id}));
	},

	validatePassword: function() {
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.passwordInsufficientlyComplex) {
						userManagerController.userViewSetPasswordProblems.innerHTML = "";
						for (let problem in response.passwordInsufficientlyComplex) {
							userManagerController.userViewSetPasswordProblems.innerHTML = userManagerController.userViewSetPasswordProblems.innerHTML + "<p>" + response.passwordInsufficientlyComplex[problem] + "</p>" 
						}

					} else {
						userManagerController.userViewSetPasswordProblems.innerHTML = "";
						userManagerController.userViewSetPassword.disabled = "";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-password-change-validation.php", true);
		request.send(JSON.stringify({newPassword: userManagerController.userViewSetPasswordTo.value, repeatNewPassword: userManagerController.userViewSetPasswordTo.value, passwordChange: false}));
	},

	setUserPassword: function() {
		userManagerController.userSpinner.style.opacity = 1;
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userManagerController.userViewSetPasswordTo.value = "";
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.show(response.data);
					} else {
						userManagerController.userActionError.innerHTML = response.message + "<br><br>" + response.data;
						userManagerController.userActionError.style.visibility = "visible";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-change-user-password.php", true);
		request.send(JSON.stringify({user: userManagerController.retrievedUser.user.id, password: userManagerController.userViewSetPasswordTo.value}));
	},

	deleteGroup: function(groupListID) {
		var group = userManagerController.retrievedUser.groups[groupListID];
		userManagerController.userSpinner.style.opacity = 1;

		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.show(response.data);
					} else {
						userManagerController.userActionError.innerHTML = response.message;
						userManagerController.userActionError.style.visibility = "visible";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-remove-security-group.php", true);
		request.send(JSON.stringify({selectedGroup: group}));

	},

	enableUserViewAddToGroup: function() {
		if (userManagerController.userViewNewGroup.value == "" && userManagerController.userViewSelectGroup.value == "") {
			userManagerController.userViewAddToGroup.disabled = "disabled";
		} else {
			userManagerController.userViewAddToGroup.disabled = "";
		}
	},

	addSecurityGroup: function() {
		var newGroup = userViewNewGroup.value;

		for (let group in userManagerController.retrievedUser.possible_groups) {

			if (userManagerController.retrievedUser.possible_groups[group].security_group == newGroup) {
				userManagerController.show(userManagerController.retrievedUser.user.id);
				return false;
			}

		}

		if (newGroup == "") {
			newGroup = userManagerController.userViewSelectGroup.value;

			if (newGroup == "") {
				userManagerController.show(userManagerController.retrievedUser.user.id);
				return false;
			}
		}

		userManagerController.userSpinner.style.opacity = 1;

		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.show(response.data);
					} else {
						userManagerController.userActionError.innerHTML = response.message;
						userManagerController.userActionError.style.visibility = "visible";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-add-security-group.php", true);
		request.send(JSON.stringify({user: userManagerController.retrievedUser.user.id, group: newGroup}));

	},

	deleteUnlock: function() {
		userManagerController.deleteUnlockClicks ++;

		for (var i = 1; i <= 5; i++) {
			document.getElementById('deleteUnlock' + i).disabled = "disabled";
		}

		window.setTimeout(function(){
			if (userManagerController.deleteUnlockClicks >= 5) {
				document.getElementById('deleteUser').disabled = "";
				return true;
			}

			var number = Math.random()*5;
			number = Math.round(number);

			if (number > 5 || number < 1) {
				number = 5;
			}

			document.getElementById('deleteUnlock' + number).disabled = "";

		},1000);

	},

	deleteUser: function() {
		userManagerController.userSpinner.style.opacity = 1;

		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userManagerController.userActionError.style.visibility = "hidden";
						userManagerController.dismiss();
						userSearchController.search();
					} else {
						userManagerController.userActionError.innerHTML = response.message;
						userManagerController.userActionError.style.visibility = "visible";
					}

				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-delete-user.php", true);
		request.send(JSON.stringify({userID: userManagerController.retrievedUser.user.id}));
	},
}