var siteConfigController = {
	configForm: document.getElementById('configForm'),

	inputs: configForm.getElementsByTagName('input'),

	configManagementError: document.getElementById('configManagementError'),

	retrieve: function() {

		var request = new XMLHttpRequest();

		if (siteConfigController.configSpinner) {
			siteConfigController.configSpinner.style.opacity = 1;
		}
		
		request.onreadystatechange = function () {

			if (request.readyState == 4) {

				var response = JSON.parse(request.response);

				if (response.success) {
					siteConfigController.configManagementError.style = "visibility: hidden;"

					var inputs = siteConfigController.inputs;

					inputs.minimumCharacters.value = response.data.password_config.minimum_characters;
					inputs.minimumCapitalLetters.value = response.data.password_config.minimum_capital_letters;
					inputs.minimumNumbers.value = response.data.password_config.minimum_numbers;
					inputs.minimumSpecialCharacters.value = response.data.password_config.minimum_special_characters;

					inputs.maxLogins.value = response.data.throttling_config.max_login_requests;
					inputs.loginLockout.value = response.data.throttling_config.login_lockout_in_minutes;
					inputs.maxRegistrations.value = response.data.throttling_config.max_registrations_per_ip;
					inputs.registrationLockout.value = response.data.throttling_config.registration_lockout_in_minutes;
					inputs.maxPasswordResets.value = response.data.throttling_config.max_password_reset_requests;
					inputs.resetLockout.value = response.data.throttling_config.password_reset_lockout_in_minutes;

					window.setTimeout(function(){siteConfigController.configSpinner.style.opacity = 0;},2000);

				} else {
					siteConfigController.configManagementError.innerHTML = response.message;
					siteConfigController.configManagementError.style = "visibility: visible;"
				}
			}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-config-data-retrieval.php", true);
		request.send();
	},

	submitPasswordConfig: function() {
		var inputs = siteConfigController.inputs;
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
			if (request.readyState == 4) {
				var response = JSON.parse(request.response);

				if (response.success) {
					siteConfigController.configManagementError.style = "visibility: hidden;"
					siteConfigController.retrieve();
				} else {
					siteConfigController.configManagementError.innerHTML = response.message;
					siteConfigController.configManagementError.style = "visibility: visible;"
				}
			}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-configure-passwords.php", true);
		request.send(JSON.stringify({
			minimumCharacters: inputs.minimumCharacters.value, 
			minimumCapitals: inputs.minimumCapitalLetters.value,
			minimumNumbers: inputs.minimumNumbers.value,
			minimumSpecialCharacters: inputs.minimumSpecialCharacters.value
		}));
	},

	submitThrottlingConfig: function() {
		var inputs = siteConfigController.inputs;
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
			if (request.readyState == 4) {
				var response = JSON.parse(request.response);

				if (response.success) {
					siteConfigController.configManagementError.style = "visibility: hidden;"
					siteConfigController.retrieve();
				} else {
					siteConfigController.configManagementError.innerHTML = response.message;
					siteConfigController.configManagementError.style = "visibility: visible;"
				}
			}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-configure-throttling.php", true);
		request.send(JSON.stringify({
			maxLogins: inputs.maxLogins.value, 
			loginLockout: inputs.loginLockout.value,
			maxRegistrations: inputs.maxRegistrations.value,
			registrationLockout: inputs.registrationLockout.value,
			maxPasswordResets: inputs.maxPasswordResets.value,
			resetLockout: inputs.resetLockout.value
		}));
	},
	
};

siteConfigController.retrieve();