var updateNameFormController = {
	newName: document.getElementById('newName'),

	nameChangeError: document.getElementById('nameChangeError'),

	nameChangeSubmitError: document.getElementById('nameChangeSubmitError'),

	submit: document.getElementById('nameSubmit'),

	nameData: document.getElementById('nameData'),

	validate: function() {
		//People should be able to set their name to a null value if they want.
		nameSubmit.disabled = "";
					
	},

	submit: function() {
		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);
					
					if (response.success == false) {

						if (response.message.match("token")) {
							updateNameFormController.nameChangeSubmitError.innerHTML = "<p>" + response[1] + " Log in again to get a new token." + "</p>";
						} else {
							updateNameFormController.nameChangeSubmitError.innerHTML = "<p>" + response[1] + "</p>";
						}

					} else {
						if (response.data == "") {
							updateNameFormController.nameData.innerHTML = response.data;
						} else {
							updateNameFormController.nameData.innerHTML = response.data + ", ";
						}

						commonDashboardController.toggleDisplayOf('nameChangeWrapper');
					}
				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-name-update.php", true);
		request.send(JSON.stringify({name: updateNameFormController.newName.value}));
	},
	
};