var spinnerInjector = {
	inject: function() {
		var globalSpinner = document.getElementById('globalSpinner');
		userManagerController.userSpinner = globalSpinner;
		siteConfigController.configSpinner = globalSpinner;
	},
};

spinnerInjector.inject();