var userSearchController = {
	userSearchTerm: document.getElementById('userSearchTerm'),

	userSearchResultsTable: document.getElementById('userSearchResultsTable'),

	userSearchData: document.getElementById('userSearchData'),

	userSearchError: document.getElementById('userSearchError'),

	search: function() {
		if (userManagerController) {
			userManagerController.dismiss();
		}

		var request = new XMLHttpRequest();

		request.onreadystatechange = function () {
				if (request.readyState == 4) {
					var response = JSON.parse(request.response);

					if (response.success) {
						userSearchController.userSearchError.innerHTML = "";
						userSearchController.userSearchError.style = "visibility: hidden;"
						userSearchController.userSearchData.innerHTML = "";
						
						for (let user in response.data) {
							var dataTR = document.createElement('tr');
							dataTR.id = "userSearch-" + user;
							dataTR.addEventListener("click", function(event){
								var highlighted = document.getElementsByClassName('darkRedHighlight');

								if (highlighted.length > 0) {
									highlighted[0].className="";
								}
								
								event.target.parentElement.className="darkRedHighlight";
								userManagerController.show(response.data[user].id)
							}, false);

							var idTD = document.createElement('td');
							idTD.innerHTML = response.data[user].id;
							dataTR.appendChild(idTD);

							var nameTD = document.createElement('td');
							nameTD.innerHTML = response.data[user].name;
							dataTR.appendChild(nameTD);

							var emailTD = document.createElement('td');
							emailTD.innerHTML = response.data[user].email;
							dataTR.appendChild(emailTD);

							userSearchData.appendChild(dataTR);

						}

					} else {
						userSearchController.userSearchError.innerHTML = response.message;
						userSearchController.userSearchError.style = "visibility: visible;"
					}
				}
		}

		request.open("POST", document.location.origin + "/ajax-logic/ajax-user-search.php", true);
		request.send(JSON.stringify({searchTerm: userSearchController.userSearchTerm.value}));
	},

	unsetHighlight: function() {
		var data = document.getElementById('userSearchData');
		var highlighted = data.getElementsByClassName('darkRedHighlight');
		
		for (let element in highlighted) {
			highlighted[element].className = "";
		}
	},
	
};