var registrationFormValidator = {

	request: new XMLHttpRequest(),
	
	email: function () {
		return document.getElementById('registerEmail').value;
	},

	password: function() {
		return document.getElementById('registerPassword').value;
	},

	repeatPassword: function() {
		return document.getElementById('registerRepeatPassword').value;
	},

	valid: false,

	buttonWasDisabledOnInit: false,

	//Delayed validate is a request throttler - if not in place, registration password entries would fire a request with every keystroke.
	delayedValidate: function() {
		
		clearTimeout(window.lastTimer);

		window.lastTimer = setTimeout(function() {
			registrationFormValidator.validate("delayed");
		}, 750);
		
	},

	validate: function(blur) {

		if (blur == "blur" || blur == "delayed") {
			this.request.open("POST", document.location.origin + "/ajax-logic/ajax-registration-validation.php", true);
		
			this.request.onreadystatechange = function () {

				function multiProblemStringDisplay(element, problemArray) {

					var string = "";
					
					for (let problem in problemArray) {
						string = string + "<p>" + problemArray[problem] + "</p>";
					}

					element.innerHTML = string;

				}
				
				var validator = registrationFormValidator;

				if (validator.request.readyState == 4) {
					var response = JSON.parse(validator.request.response);
					validator.valid = true;

					for (let property in response) {
						
						if (response[property]) {
							validator.valid = false;
						}

					}

					if (!validator.buttonWasDisabledOnInit) {

						if (!validator.valid) {
							validator.button.disabled = "disabled";
						} else {
							validator.button.disabled = "";
						}

					}

					if (!validator.valid) {

						multiProblemStringDisplay(validator.emailRegistrationError, response.emailProblems);
						
						if (validator.password() == "") {
							validator.passwordRegistrationError.innerHTML = "";
						} else {
							multiProblemStringDisplay(validator.passwordRegistrationError, response.passwordInsufficientlyComplex);
						}

						if (validator.repeatPassword() == "") {
							validator.repeatPasswordRegistrationError.innerHTML = "";
						} else if (response.passwordsUnmatched) {
							validator.repeatPasswordRegistrationError.innerHTML = "<p>The two password entries don't match.</p>";
						} else {
							validator.repeatPasswordRegistrationError.innerHTML = "";
						}

					} else {
						validator.emailRegistrationError.innerHTML = "";
						validator.passwordRegistrationError.innerHTML = "";
						validator.repeatPasswordRegistrationError.innerHTML = "";
					}

				}
			};

			function sendRequest() {
				var validator = registrationFormValidator;
				validator.request.send(JSON.stringify({email: validator.email(), password: validator.password(), repeatPassword: validator.repeatPassword()}));
			}

			sendRequest();
		}

	},

};

registrationFormValidator.button = document.getElementById('registerButton');
registrationFormValidator.emailRegistrationError = document.getElementById('emailRegistrationError');
registrationFormValidator.passwordRegistrationError = document.getElementById('passwordRegistrationError');
registrationFormValidator.repeatPasswordRegistrationError = document.getElementById('repeatPasswordRegistrationError');

if (registrationFormValidator.button.disabled == "disabled") {
	registrationFormValidator.buttonWasDisabledOnInit = true;
}

registrationFormValidator.button.disabled = "disabled";