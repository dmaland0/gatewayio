var newPasswordFormValidator = {

	newPassword: document.getElementById('newPassword'),

	passwordRegistrationError: document.getElementById('passwordRegistrationError'),

	repeatNewPassword: document.getElementById('repeatNewPassword'),

	repeatPasswordRegistrationError: document.getElementById('repeatPasswordRegistrationError'),

	submit: document.getElementById('newPasswordButton'),
	
	validate: function() {
		
		if (this.newPassword.value == "") {
			this.submit.disabled = "disabled";
		} else {

			if (this.newPassword.value != this.repeatNewPassword.value) {
				this.submit.disabled = "disabled";
			}

			clearTimeout(window.lastTimer);

			window.lastTimer = setTimeout(function() {
				var request = new XMLHttpRequest();
				var target = document.location.origin + "/ajax-logic/ajax-password-change-validation.php";
				request.open("POST", target, true);

				request.onreadystatechange = function() {
					
					if (request.readyState == 4) {
						var response = JSON.parse(request.response);
						
						if (response.passwordInsufficientlyComplex) {
							var string = "";

							for(let problem in response.passwordInsufficientlyComplex) {
								string = string + "<p>" + response.passwordInsufficientlyComplex[problem] + "</p>";
							}

							newPasswordFormValidator.passwordRegistrationError.innerHTML = string;

						} else {
							newPasswordFormValidator.passwordRegistrationError.innerHTML = "";
						}

						if (response.passwordsUnmatched && repeatNewPassword.value != "") {
							newPasswordFormValidator.repeatPasswordRegistrationError.innerHTML = "<p>The passwords you entered don't match.</p>";
						} else {
							newPasswordFormValidator.repeatPasswordRegistrationError.innerHTML = "";
						}

						if (response.passwordInsufficientlyComplex || response.passwordsUnmatched) {
							newPasswordFormValidator.submit.disabled = "disabled";
						} else {
							newPasswordFormValidator.submit.disabled = "";
						}
					}

				};

				request.send(JSON.stringify({newPassword: newPasswordFormValidator.newPassword.value, repeatNewPassword: newPasswordFormValidator.repeatNewPassword.value, passwordChange: true}));

			}, 750);

		}

	},
};

newPasswordFormValidator.validate();