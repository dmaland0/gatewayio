var resetPasswordFormValidator = {

	email: document.getElementById('email'),

	emailError: document.getElementById('emailError'),

	submit: document.getElementById('resetPasswordButton'),
	
	validate: function() {
		
		if (this.email.value == "") {
			this.submit.disabled = "disabled";
		} else {
			clearTimeout(window.lastTimer);

			window.lastTimer = setTimeout(function() {
				var request = new XMLHttpRequest();
				var target = document.location.origin + "/ajax-logic/ajax-email-validation.php";
				request.open("POST", target, true);

				request.onreadystatechange = function() {
					
					if (request.readyState == 4) {
						var response = JSON.parse(request.response);

						if (!response.emailProblems) {
							resetPasswordFormValidator.emailError.innerHTML = "";
							resetPasswordFormValidator.submit.disabled = "";
						} else {

							if (response.emailProblems[0].match("valid")) {

								resetPasswordFormValidator.emailError.innerHTML = "<p>This doesn't seem to be a valid email.</p>";

							} else {
								resetPasswordFormValidator.emailError.innerHTML = "";
							}

							if (response.emailProblems[0].match("valid")) {

								resetPasswordFormValidator.submit.disabled = "disabled";

							} else {
								resetPasswordFormValidator.submit.disabled = "";
							}
						}

					}

				};

				request.send(JSON.stringify({email: resetPasswordFormValidator.email.value}));

			}, 750);
		}

	},

};

resetPasswordFormValidator.validate();