<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/password-reset-logic.php';
?>
<!doctype html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>
		<div class="content">
			<?php 
				$password_reset = new PasswordReset();
				$result = $password_reset->reset($_POST["email"]);

				if ($result['success'] == false) {
					include $root_directory . '/../partials/password-reset-failed.php';
				} else {
					include $root_directory . '/../partials/password-reset-success.php';
				}

			?>
		</div>
	</body>
</html>
