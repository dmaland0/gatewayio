<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/password-change-logic.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$authenticator = new AuthenticationLogic();
	$useToken = true;
	$userAuthenticated = $authenticator->authenticate($useToken);
?>
<!doctype html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>
		<div class="content">
			<?php 
				//The AuthenticationResponses "interceptor" is not used here. It would prevent continued execution due to the user's password having to be changed - but changing the password is exactly what we're trying to do!
				if ($userAuthenticated['success'] == true) {
					$changePasswordLogic = new PasswordChangeLogic();

					$changePasswordResult = $changePasswordLogic->changePassword($userAuthenticated['user_ID'], $_POST['newPassword'], $_POST['repeatNewPassword']);

					if (count($changePasswordLogic->problems) > 0) {
						include $root_directory . '/../partials/change-password-problems.php';
					} else {
						include $root_directory . '/../partials/password-update-success.php';
					}

				} else {
					include $root_directory . '/../partials/token-authentication-failed.php';
				}

			?>
		</div>
	</body>
</html>