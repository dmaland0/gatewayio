<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/authentication-logic.php';
	include_once $root_directory . '/../logic/authentication-responses.php';
	include_once $root_directory . '/../database-connection.php';

	$authenticator = new AuthenticationLogic();
	$userAuthenticated = $authenticator->authenticate(false);
?>
<!doctype html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>
		<div class="content">
			<?php
				$authentication_responses = new AuthenticationResponses();

					$authentication_responses->authenticationResponseHandler($userAuthenticated);

					if ($authentication_responses->successfulAuthentication) {
						$connect = new DatabaseConnection();
						$action = $connect->connection->prepare("DELETE FROM users WHERE id = ?");
						$action->execute(array($userAuthenticated['user_ID']));
						include $root_directory . '/../partials/account-delete-success.php';
					}
			?>
		</div>
	</body>
</html>
