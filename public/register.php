<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../logic/registration-logic.php';
?>

<!doctype html>
<html lang="en">
	<head>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>

		<?php
			$logic = new RegistrationLogic();

			if (count($logic->problems) > 0) {
				include $root_directory . "/../partials/registration-problems.php";
			}

			if ($logic->success[0] == "throttled") {
				include $root_directory . "/../partials/registration-throttled.php";
			}

			if ($logic->success[0] == "success") {
				include $root_directory . "/../partials/registration-success.php";
			}

			if ($logic->success[0] == "bailout") {
				include $root_directory . "/../partials/registration-bailout.php";
			}

		?>
		
	</body>
</html>
