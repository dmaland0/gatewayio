<?php
	$root_directory =  $_SERVER['DOCUMENT_ROOT'];
	include_once $root_directory . '/../database-connection.php';
	include_once $root_directory . '/../logic/admin-registration-query.php';
	include_once $root_directory . '/../logic/authentication-logic.php';

	$database_connection = new DatabaseConnection();

	if (isset($_POST['user_id'])) {
		$authentication_logic = new AuthenticationLogic();
		$authentication_logic->setACookie("Logged Out");
	}
?>

<!doctype html>
<html lang="en">
	<head>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" href="styles/global.css">
	</head>
	<body>

		<div id="loginBox">
		<!-- Replace this include (or modify logo.php) to use your own logo. -->
			<?php 
				include $root_directory . '/../partials/logo.php';
			?>

			<div id="formWrapper">
			
				<form name="loginForm" id="loginForm" class="foundationForm frontPageForm" action="dashboard.php" method="post">
			  		<label>Email</label>
			  		<br>
					<input type="text" name="loginEmail" id="loginEmail">
					<br>
					<label>Password</label>
					<br>
					<input type="password" name="loginPassword" id="loginPassword">
					<br>
					<input type="submit" value="Log In" id="loginButton" class="customButton"
						<?php 
							if (!$database_connection->connection) {echo "disabled=\"disabled\"";}
						?> >
					<br>
					<label>
  						<a href="password-reset-form.php">Password reset...</a>
  					</label>
				</form>
				
				<form name="registerForm" id="registerForm" class="foundationForm frontPageForm" action="register.php" method="post">
					<label>Email</label>
			  		<br>
					<input type="text" name="registerEmail" id="registerEmail" onblur="registrationFormValidator.validate('blur')">
					<br>
					<div class="registrationFormError" id="emailRegistrationError"></div>
					<label>Password</label>
					<br>
					<input type="password" name="registerPassword" id="registerPassword" oninput="registrationFormValidator.delayedValidate()">
					<br>
					<div class="registrationFormError" id="passwordRegistrationError"></div>
					<label>Repeat Password</label>
					<br>
					<input type="password" name="registerRepeatPassword" id="registerRepeatPassword" oninput="registrationFormValidator.delayedValidate()">
					<br>
					<div class="registrationFormError" id="repeatPasswordRegistrationError"></div>
					<input type="submit" value="Register" id="registerButton" class="customButton"
						<?php 
							if (!$database_connection->connection) {echo "disabled=\"disabled\"";}
						?> >
				</form>
				
				<div class="clear"></div>
			
			</div>

			<?php 
				$admin_registration = new AdminRegistrationQuery();

				if($admin_registration->admin_can_be_registered) {
					include $root_directory . '/../partials/register-admin-message.php';
				}
			?>

			<div id="databaseStatus">
				<p>Database Status</p>
				<span id="response">

					<?php
						echo $database_connection->connection_details;
					?>

				</span>
			</div>
		</div>
		</div>
		<script src="/scripts/registration-form-validator.js"></script>
	</body>
</html>
