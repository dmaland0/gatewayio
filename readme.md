GatewayIO
=========                                    

A web application foundation created by Danny Maland of BlueDoor Software LLC
-----------------------------------------------------------------------------

[My Patreon Page](https://www.patreon.com/danielmaland)

---
### Important License-ish Information (Don't Skip This)

You are free to use GatewayIO for whatever you like, to modify it in any way you like, and also to release your modifications.

I want credit for building this thing, though, so take that into account! :)

GatewayIO is available free of charge, and also free of any warranty. It's "as is," in other words. I do not want this software to misbehave, wreck your server, be insecure, or otherwise fail, but I:

* Can't guarantee it won't ever do those things.

* Can't be legally reponsible if it does.

Please use GatewayIO at your own risk, or don't use it at all.

---

### What Is GatewayIO?

GatewayIO is a sort of web application "bootstrap" package. It is designed to handle things like user registration, authentication, security group assignments, and permissions. The idea is that these basic underpinnings are figured out for you, so that you don't have to do them yourself to simply get started on a project.


### What Is GatewayIO NOT?

You've probably noticed that GatewayIO is called a web application foundation, and not called a framework. There's a good reason for this. A foundation (in terms of physical buildings) is much more basic than the framing. A foundation is meant to be a solid place to set things down - and that's about it.

A proper framework pre-defines a LOT of things, like how you interact with a database, whether or not the application is always meant to use asynchronous calls to the backend, and what kinds of "helper" functionalities are included. This can be very, very useful, and real frameworks have an established and rightful place in application development.

Frameworks do have downsides, though, especially as they get larger and more complex. The bigger they get, the more they seem to force you and your application into their specific way of doing things. In theory, any framework that uses an interpreted language can be "hacked," but let's be honest: Digging through the source and tweaking things is non-trivial and might break something unexpectedly. If you don't like a small part of the framework, you're basically stuck. The framework "does it this way," and if your mind doesn't understand the problem that way, life gets hard. You sometimes have to bend and contort your project's behavior to fit the framework.

Other issues include bloat, tangled dependencies, middleware that can sometimes do strange things (or just break unexpectedly), documentation that doesn't quite cover what you need to figure out your specific problem...the list goes on.

The point of GatewayIO is (hopefully) to NOT do these things to you.

Another important thing to note is that GatewayIO is meant to be highly modular, as well as being relatively picky about things like user authentication. This is as opposed to being built for hyper-speed by default. If you're definitely planning on having thousands of concurrent users, and you need everything to run like lightning, this may not be the application foundation for you.


### Who Is GatewayIO For, Then?

GatewayIO is for people who either know, or are willing to figure out, what they're doing and what they want. It's for people who want to have a real shot at understanding EXACTLY how everything in their application works. Relative to a full framework, GatewayIO should be stomp-down easy to understand, take apart, modify, cut down, and build on.

GatewayIO is for people who want a minimal number of assumptions forced on a project. There's no templating system that you absolutely must use. This isn't a blogging platform trying to be all things to all people - although it could be, if you did enough work on it. Vanilla PHP and SQL should be all that's needed to make the backend do its thing, or do your things.

This is a web application foundation for people who want to run their own show and make as many of their own choices as possible.

---

Getting Started
---------------


### What You Need

You will need a web host or local dev environment that can handle MySQL databases and PHP with the PDO extension. The super whiz-bang features of the very latest versions shouldn't be necessary, but a host that pays attention to having recent, stable installs of things is always a plus.

If, somewhere along the line, you get an error that a mysterious function is not found, unavailable, or undefined, your version of PHP may be too old.

Cloning down the GatewayIO repository, or otherwise getting a copy of the files into a servable directory should be all you need to begin making things functional.


### How To Get Moving

**Danger! The database creation script will ruthlessly and efficiently wipe out critical data in any schema named "gateway" on your MySQL server. If you want to run multiple GatewayIO applications from the same database server, or you already have a "gateway" schema that does something else, you MUST replace all of the creation script's internal references to "gateway" with another schema name!**

In the root level of the GatewayIO folder, there should be an SQL file called gateway_db_structure.sql. This SQL file contains the commands to create a database structure that the application foundation expects to find. Running those commands on your MySQL server (in whatever way makes sense to you) should result in a new, empty database schema called "gateway."

It is strongly suggested that you use a database management tool to create a database user which has only the minimum permissions necessary for your application to work. For most applications, the user that the application connects as should be able to select, insert, update, delete, execute, and show views, but should NOT be able to do anything that involves changing the database structure or granting database-level rights to other users. (Other types of applications may require more expansive privileges, but that's beyond the scope of just getting rolling.)

At this point, you can edit the configuration section of database-connection.php such that it matches your MySQL server's configuration. The `$dbname` variable doesn't necessarily have to be changed, because the generated database schema's name should be "gateway." (Of course, if you changed that around, then you will have to alter `$dbname`.) Having your server setup so that the other defaults DO have to be changed is definitely encouraged - otherwise, you could be providing a bit of a backdoor into your database for folks who want to try connecting with the defaults.

The "public" directory is what the root of your site should be according to the webserver. "Public" is a subdirectory of the foundation's root directory, because certain files - like database-connection.php - should NOT be able to be navigated to by a regular user. The idea here is that all files related to application execution DO need to have permissions such that PHP can execute them, but that files with configuration information and secrets should not be world-browsable.

If everything's in place, your database is up, and the webserver is running, you should be able to browse to your site and get the default view (index.php) handed to you. If you follow the link to "Login/ Register" and there's a problem with the database connection, GatewayIO tries to give you some clues by echoing the PDO exception to the "Database Status" page block.


### An Important Overall Security Note
"Server-level" security is not what GatewayIO does. For instance, if you're going to have users doing anything (at all) that could cause harm to them if the information involved were to be intercepted, it is YOUR responsibility to have that traffic use a secure connection. This application foundation can't do that for you.

This is also one of the reasons why GatewayIO only does a minimal amount with email notifications as a "built in" operation. Email notifications are a pretty decent way of communicating with users and verifying that account activity is legitimate. However, sending emails securely is not something that GatewayIO can definitely, unquestionably do without knowing in advance exactly how you intend to send those emails.

You are ultimately the one who has to figure out how to handle data exchange in the way that's best for your users. Nobody else can do that for you.

**As a general rule, assume that anything you transmit can be read by the end user in some way. Hiding data from the frontend view does NOT guarantee that the data can't be accessed. If it arrived at the user's browser, the browser can be made to display it.**


### Basic Configuration

If you look at your Gateway database with your favorite database management tool, you'll find a table called "password_complexity_configuration." This table is where you set the "minimum acceptables" for system passwords. Some defaults should already be set for you.

All the fields are integers, and unmodified GatewayIO code will only ever look at the first row. All values must be non-null, equal to or greater than 0, and the individual character-type minimums should not sum to more than the minimum_characters field. (It doesn't make sense to say that you require a minimum total length of "n" characters, but then to require a mix of capital letters, numbers, and special characters that is greater than "n.")

Also in your database should be a table named "throttling_configuration." This is where you set how many attempts at logging-in, registering, and re-setting passwords are allowed before the system rejects further attempts. It's also where you set the associated lockout times. As with password_complexity_configuration, there's only supposed to be one row of values, and all values should be integers. This table should have some preset values.


### Setting Up A Root/ Admin User

If there are zero users registered in GatewayIO with the "admin" security group, you can register a user that will automatically get admin privileges.

Doing this requires using your favorite database management tool to make entries in the "expected_email" and "expected_passwords" fields of the table "admin_registration." (There should only ever be one row in admin_registration.) If you make this entry, GatewayIO will always try to match a registration attempt with these stored credentials. Any attempt that doesn't match will be rejected.

**Be aware that the expected_password field must conform to the complexity standards set in "password_complexity_configuration." The expected_password field is NOT an override on those settings.**

If a user registers with the correct credentials, they will automatically get an "admin" entry in the security_groups table...and their "must_change_password" flag will be set to true. The entry in admin_registration will be deleted.

**This is obviously a bit of a security hole. If there's a significant chance that the general public will be able to try registering for your application while the admin auto-registration option is available, you might want to avoid putting anything in admin_registration.**

Of course, a regular user can also be put in the "admin" security group manually by directly changing your database.

Once you have a user with admin privileges, you might also want to register a "regular" user. Having both kinds of users available on the system will mean you can test the operation of your code for both general cases.


Developing On GatewayIO
-----------------------


### The Basics

As said before, GatewayIO is built on the idea that you want to know pretty much exactly what's going on. For this reason, and also because re-wrapping all the functionality (without bastardizing it) would just turn GatewayIO into a clone of other projects, you won't find all kinds of wrappers and middleware for things like database operations. You're going to need to know some SQL in order to make things work.

**Remember to use prepared statements! Safety is no accident.**

---

The overall GatewayIO philosophy is, "If you want it, include_once. If you don't want it, don't." The most fundamental example of this is including database-connection.php:

```php
<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];

include_once $root_directory . '/../database-connection.php';
```

With the file included, a variable set equal to `new DatabaseConnection()` now becomes a usable tool for database activities. The variable should now be an object with a "connection" property, that property being an actual database connection object that you can call SQL operations (like `prepare()`) on.

For example, here's a snippet from ajax-config-data-retrieval.php:

```php
<?php

$connect = new DatabaseConnection();
$connection = $connect->connection;

$query = $connection->prepare("SELECT * FROM password_complexity_configuration");
...
```

---

Some additional notes:

1) Defining $root_directory as the `$_SERVER['DOCUMENT_ROOT']` means that, no matter where a PHP file is, a fixed reference point can be used for all includes. This neatly prevents all kinds of problems caused by relative-path includes being called from files that are located in all kinds of different places. You don't have to do this, of course, but it might make your life easier in the long run.

2) Using include_once means that modularity is increased. It's entirely possible to have a PHP file that's expected to perform database operations in isolation, and so includes database-connection.php. But, what if it would be helpful to include the "isolated operations" file into another file that also included database-connection.php? With a regular include, you get an error when PHP attempts to suddenly redefine the DatabaseConnection class. With include_once used in both files, database-connection.php is loaded only if it's absent.

3) `$_SERVER['DOCUMENT_ROOT']` is a directory name without a trailing slash, so an initial slash needs to be included in the path concatenated to `$root_directory`. 

4) Because database-connection.php contains "secrets" (like the database username and password), it does NOT sit in the publicly browsable directory. It lives one level up, and so requires ../ to be found. Files inside the site root don't have this issue, of course.

These basic ideas can be applied to all other includes you end up using in your project.


### Email Sender

As stated before, GatewayIO tries to do a minimum with email. However, having a meaningful password-reset system is basically impossible without it. As such, a utility for sending PHP mail is included, and lives in its own file so that you can reconfigure it easily. **PHP mail REQUIRES that you have sendmail installed on your server. It will not work otherwise**

Including Email Sender looks something like this:

```php
<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];

include_once $root_directory . '/../logic/email-sender.php';
```

Of course, the only thing to really configure is the `$headers` variable. **Make sure that you set it appropriately for your server!**

Using email sender is very simple:

```php
<?php
$some_variable = EmailSender::send($email_address, $subject, $message);
```

`EmailSender::send` will set `$some_variable` to a human-readable message indicating whether the email appears to have been sent or not.

**Be warned! Just because an email appears to have been sent is no indication that it was sent with encryption enabled, or that it arrived. Testing any and all email functionality thoroughly on your system is your responsibility.**


### Authentication

For any page that you want to restrict to authenticated users, simply make sure that, as early as you can, you add:

```php
<?php
include_once $root_directory . '/../logic/authentication-logic.php';

$authenticator = new AuthenticationLogic();

$userAuthenticated = $authenticator->authenticate(true);
```

**Authentication needs to set cookies to work correctly, so the call to `authenticate()` MUST come before any other output to the browser.**

The parameter to `authenticate()` indicates whether or not to attempt authentication with a pre-existing authorization token. Passing false (or a variable interpretable as false) indicates that the only acceptable login method is from a POST operation containing loginEmail and loginPassword fields.

The `authenticate()` method returns an array with the following keys:

`['success', 'message', 'user_ID', 'user_must_change_password', 'security_groups']`

---

Once you have a return value from `authenticate()`, it's up to you to do something with it. The easy way to make use of that value is through:

1) Putting the following
```php
<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];

include_once $root_directory . '/../logic/authentication-responses.php';
```
(or whatever variation of the above makes sense to you) into the page, and then...


2) In the HTML section of the page, adding a PHP block like this:

```php
<?php
$authentication_responses = new AuthenticationResponses();

$authentication_responses->authenticationResponseHandler($userAuthenticated);

	if ($authentication_responses->successfulAuthentication) {
		
		include some-content-requiring-authentication.php;

	}
```

What this does is "intercept" your page content if an authentication problem occurs. The `authenticationResponseHandler()` method injects an error message into the page if something is amiss, and sets its `successfulAuthentication` property to be false. When that property is false, anything within the trailing "if" block is suppressed.

Of course, anything you put outside the "if" block will be rendered as normal.

---

A handy function available in `authentication-logic.php` is `userIsInSecurityGroup`. It takes a single parameter, which is the name of the security group being tested for. Based on the authentication token supplied by the logged-in user, the function returns either `true` or `false`. The point of this function is to make it easier to figure out and control if a user should have access to something. For example:

```php
<?php
if ($authentication->userIsInSecurityGroup("admin")) {
    do_something();
} else {
    do_something_different();
}
```

Please note that `userIsInSecurityGroup` does, in fact, re-authenticate the logged-in user based on their security token.

### Password Hashing

Password hashing is very simple, and required if you want to update passwords or compare input against the database. The include looks like this:

```php
<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];

include_once $root_directory . '/../logic/password-hasher.php';
```

Once that's in, password hashing is a call to a static function:

```php
<?php
$hashed_password = PasswordHasher::hashPassword($password_to_hash, $salt_value);
```

Of course, if you need a user's pre-existing salt (because you're updating or matching their password), you'll have to query the database for it separately.


### AJAX

The basic operations you need for AJAX **are** available in a wrapped form. This is because, unlike database operations, AJAX JSON input and output handling is just a bunch of rote busywork - and it's really simple to create convenience functions that alleviate the annoyance.

```php
<?php
$root_directory =  $_SERVER['DOCUMENT_ROOT'];

include_once $root_directory . '/../logic/ajax-operations.php';
```

Including ajax-operations.php into a file lets you use `$variable_name_here = ajaxOperations::readJSON();` to parse JSON out of the php://input stream. As such, the assigned variable should become a standard PHP object.

Also available are `ajaxOperations::respondWithJSON($variable_to_respond_with)` and `ajaxOperations::respondWithRaw($variable_to_respond_with)`. As the names imply, one will attempt to construct and respond with a string that can be parsed as JSON, while the other just attempts to respond with a simple `print_r()` string.

---

One other feature of `ajax-operations.php` is the "response" class. The response class is meant to be a quick shorthand for building consistent AJAX replies, like you might see in `ajax-name-update.php`:

```php
<?php
$response = new response(true, "The name was updated", $user->name);
...
ajaxOperations::respondWithJSON($response);
```

The arguments to the `response` constructor:
`(boolean $operation_success, string $message_explaining_results, mixed $data_to_respond_with)`

As always, you are not required to use `response`. It's just there to make certain things easier.

---

Other Whys And Wherefores
-------------------------


### Password Salts

GatewayIO does indeed salt passwords, appending a random string to password data when it's received - but why store the salt in the "users" table, where it could be viewed by an attacker who has stolen the database?

It would indeed be theoretically better to store password salts in some file only accessible by PHP itself. However, at some point, the reality sets in that an attacker who managed to get a complete copy of the users table would probably also be able to get their hands on the salt file anyway. The point of salting the passwords is that, along with password hashing, the difficulty of cracking any individual password goes up.

Let's say that our nefarious attacker has the users table, and a separate list of hash outputs for common passwords. Without salting, their list of hash values is very useful. They just look for matches with the password column, and if they find one, they know the cleartext value of the password. With a whole bunch of per-user salts, their lookup list is drastically devalued. They can't just look for a match in a simple way. Rather, they have to try appending the per-user salt to every common password they can think of, getting a hash value each time to see if there's a match in the users table.

And, with a per-user salt, they have to repeat the process for each user individually.

As more and more processing power becomes available, this type of security does decrease in value. However, for any finite amount of computational muscle, there is still a non-zero benefit.


### Authentication Tokens

An examination of the authentication tokens generated by authentication.php reveals that they are SHA256-hashed amalgams of the login time and 20 pseudo-random characters. By default, a new token is generated whenever a user signs in with the login form, and the token expires as soon as the user's browser session is terminated.

To be an effective security measure, the token is highly reliant on an encrypted connection being used. Copying a cookie sent in cleartext is, of course, a trivial matter. However, if an encrypted connection prevents an attacker from directly copying the cookie, it should be pretty challenging to forge one. An attacker would first have to know (or guess) the exact "UNIX time" that a user logged on, and then correctly guess the equivalent of 20 characters. Add to this that the token should change fairly often - the token is always set on a manual login, and a manual login should be required for each session. Also add that the token-based authenticator doesn't accept tokens from IP addresses that appear to have changed midstream, and a successful attack against an authentication token seems sufficiently troublesome to deter a casual attacker.

A truly determined attacker, on the other hand, will find a way to get in.


### Behavioral Inconsistency

Some GatewayIO actions require a user to travel "through" a PHP endpoint, whereas other actions are handled asynchronously. There are a few reasons for this:

1) At the outset, I desired that basic interaction with "public facing" elements should not strictly require Javascript. However, when someone is logged into the "trusted" area of the site, it seemed reasonable that a user might be more willing to enable Javascript.

2) At some point it stops making sense to effectively develop everything twice - once for a no-script environment, and again for an environment with scripting available. This is especially true for "smaller" features.

3) I wanted to avoid using Javascript to change the user's "state." At least as I've seen it handled, it's very unwieldy and complicated compared to just having the server deal with it. As such, AJAX logins and navigation don't make much sense to me.

4) There's no good reason why registration couldn't work perfectly well through AJAX, with a success message telling the user that they can log in. I didn't think of that when I was building the registration system, though, and I don't want to go back and redo it just now. So there.

Also, speaking in general, the development for this thing wasn't exactly mapped out in advance. As such, a single, strict design pattern isn't imposed on the project as of now.